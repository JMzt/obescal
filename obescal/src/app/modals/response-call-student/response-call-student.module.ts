import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResponseCallStudentPage } from './response-call-student.page';

const routes: Routes = [
  {
    path: '',
    component: ResponseCallStudentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ResponseCallStudentPage]
})
export class ResponseCallStudentPageModule {}
