import { Component, OnInit, Input } from '@angular/core';
import { ToastController, ModalController } from '@ionic/angular';
import { Appel } from 'src/app/interfaces/Appel';
import { AppelService } from 'src/app/services/appel.service';
import { of } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-response-call-student',
  templateUrl: './response-call-student.page.html',
  styleUrls: ['./response-call-student.page.scss'],
})
export class ResponseCallStudentPage implements OnInit {

  @Input() distance: number;
  @Input() userId: string;
  @Input() call: Appel;
  public code: string;

  constructor(private toastCtrl: ToastController, private callsService: AppelService, private modalCtrl: ModalController, private afAuth: AngularFireAuth) { }

  ngOnInit() {
    this.afAuth.authState.subscribe((auth) => {
      if (auth) {
        this.userId = auth.uid;
      }
    });
  }

  /** Display a toast message */
  private async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  public validate(call: Appel, code: string) {
    console.log(this.userId)
    if (call.codeUnique === code) {
      this.callsService.answerToCall(call, true, this.userId).then(() => {
        this.presentToast('Vérification réussie.');
        this.modalCtrl.dismiss();
      });
    } else {
      this.presentToast('Le code unique saisie est incorrect.')
    }
  }

}
