import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCoursePage } from './new-course.page';

describe('NewCoursePage', () => {
  let component: NewCoursePage;
  let fixture: ComponentFixture<NewCoursePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCoursePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCoursePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
