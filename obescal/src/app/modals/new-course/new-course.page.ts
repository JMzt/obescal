import { Component, OnInit } from '@angular/core';
import { CoursesService } from 'src/app/services/courses.service';
import { Course } from 'src/app/interfaces/Course';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-new-course',
  templateUrl: './new-course.page.html',
  styleUrls: ['./new-course.page.scss'],
})
export class NewCoursePage implements OnInit {
  public courseName: string;
  constructor(private courseService: CoursesService, private modalController: ModalController, private toastController: ToastController) { }

  ngOnInit() {
    
  }

  public addCourse(){
    this.courseService.createCourse({name: this.courseName} as Course);
    this.presentToast();
    this.closeModal();
  }

  public closeModal(){
    this.modalController.dismiss();
  }

  async presentToast(){
    const toast = await this.toastController.create({
      message: 'Nouveau cours : '+ this.courseName,
      position: 'top',
      duration: 2000,
    });
    toast.present();
  }

}
