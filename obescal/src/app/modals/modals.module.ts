import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FirstConnectPageModule } from './first-connect/first-connect.module';
import { CallListAbsentsPageModule } from './call-list-absents/call-list-absents.module';
import { NewPromotionPageModule } from './new-promotion/new-promotion.module';
import { NewCoursePageModule } from './new-course/new-course.module';
import { NewUserPageModule } from './new-user/new-user.module';

// Obescal components

@NgModule({
    declarations: [],
    entryComponents: [],
    imports: [IonicModule, FormsModule, CommonModule, FirstConnectPageModule, CallListAbsentsPageModule, NewPromotionPageModule, NewCoursePageModule, NewUserPageModule],
    exports: []
})
export class ModalsModule { }
