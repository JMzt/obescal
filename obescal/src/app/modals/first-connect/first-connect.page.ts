import { ModalController } from '@ionic/angular';
import { User } from './../../interfaces/User';
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-first-connect',
  templateUrl: './first-connect.page.html',
  styleUrls: ['./first-connect.page.scss'],
})

export class FirstConnectPage implements OnInit {

  @Input() user: User;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() { }

  validate() {
    this.modalCtrl.dismiss(this.user);
  }

}
