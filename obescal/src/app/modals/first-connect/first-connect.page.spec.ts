import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstConnectPage } from './first-connect.page';

describe('FirstConnectPage', () => {
  let component: FirstConnectPage;
  let fixture: ComponentFixture<FirstConnectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FirstConnectPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstConnectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
