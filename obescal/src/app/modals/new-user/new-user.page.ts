import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/interfaces/User';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.page.html',
  styleUrls: ['./new-user.page.scss'],
})
export class NewUserPage implements OnInit {
  @Input() type: string;
  labelInput: string;
  newUser: User;

  constructor(private userService: UsersService, private modalController: ModalController, private toastController: ToastController) { }

  ngOnInit() {
  }
  public addUser(): void {
    this.userService.createAccount(this.newUser);
    this.presentToast();
    this.closeModal();
  }
  public closeModal(){
    this.modalController.dismiss();
  }

  async presentToast(){
    const toast = await this.toastController.create({
      message: 'Nouvelle promotion : '+ this.labelInput,
      position: 'top',
      duration: 2000,
    });
    toast.present();
  }
}
