import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CallListAbsentsPage } from './call-list-absents.page';

const routes: Routes = [
  {
    path: '',
    component: CallListAbsentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CallListAbsentsPage]
})
export class CallListAbsentsPageModule {}
