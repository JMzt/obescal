import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallListAbsentsPage } from './call-list-absents.page';

describe('CallListAbsentsPage', () => {
  let component: CallListAbsentsPage;
  let fixture: ComponentFixture<CallListAbsentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallListAbsentsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallListAbsentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
