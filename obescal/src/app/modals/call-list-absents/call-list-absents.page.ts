import { Component, OnInit, Input } from '@angular/core';
import { Appel } from 'src/app/interfaces/Appel';
import { ModalController } from '@ionic/angular';
import { User } from 'src/app/interfaces/User';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-call-list-absents',
  templateUrl: './call-list-absents.page.html',
  styleUrls: ['./call-list-absents.page.scss'],
})
export class CallListAbsentsPage implements OnInit {

  @Input() call: Appel;
  students=[] as User[];

  constructor(private modalController: ModalController, private usersService: UsersService) { }

  ngOnInit() {
    console.log("init ::: "+this.call.profName);
    console.log(this.call.studentsList);
    this.call.studentsList.forEach(element => {
      this.students.push(element.student as User);
    });
    //this.students = this.call.studentsList.;
  }

  public close(){
    this.modalController.dismiss(this.call);
  }

  public getPresenceStatus(stu: User){
    this.students.forEach(stud => {
      this.call.studentsList.forEach(element => {
        if (stu == element.student && element.present){
          return true;
        }
      });
      return false;
    })
    return true;
  }

}
