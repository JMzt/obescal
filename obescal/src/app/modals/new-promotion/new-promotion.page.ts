import { Component, OnInit, Input } from '@angular/core';
import { PromotionsService } from 'src/app/services/promotions.service';
import { Promotion } from 'src/app/interfaces/Promotion';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-new-promotion',
  templateUrl: './new-promotion.page.html',
  styleUrls: ['./new-promotion.page.scss'],
})
export class NewPromotionPage implements OnInit {
  
  @Input() edit: boolean;
  public promotionName: string;
  constructor(private promotionService: PromotionsService, private modalController:ModalController, private toastController: ToastController) { }

  ngOnInit() {
  }
  public addPromotion(): void {
    this.promotionService.createPromotion({name: this.promotionName} as Promotion);
    this.presentToast();
    this.closeModal();
  }
  public closeModal(){
    this.modalController.dismiss();
  }

  async presentToast(){
    const toast = await this.toastController.create({
      message: 'Nouvelle promotion : '+ this.promotionName,
      position: 'top',
      duration: 2000,
    });
    toast.present();
  }
}
