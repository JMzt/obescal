import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPromotionPage } from './new-promotion.page';

describe('NewPromotionPage', () => {
  let component: NewPromotionPage;
  let fixture: ComponentFixture<NewPromotionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPromotionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPromotionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
