import { UsersService } from './services/users.service';
import { Component, Input } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CoursesService } from './services/courses.service';
import { User } from './interfaces/User';
import { FcmProvider } from './services/providers/fcm';
import { AuthService } from 'src/app/services/auth.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})

export class AppComponent {

  @Input()
  currentUser: User;

  constructor(
    private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar, private users: UsersService,
    private courses: CoursesService, private authService: AuthService, private fcm: FcmProvider) {
    this.initializeApp();
    this.initNotifications();

    /** Create mock students */
    this.users.addAccount({ idNum: '45105', type: 'student' } as User);
    this.users.addAccount({ idNum: '45105', type: 'student' } as User);
    this.users.addAccount({ idNum: '45105', type: 'student' } as User);
    this.users.addAccount({ idNum: '45105', type: 'student' } as User);

    /** Create mock professors */
    this.users.addAccount({ idNum: '0541', type: 'professor' } as User);
    this.users.addAccount({ idNum: '256', type: 'professor' } as User);

    /** Create mock admins */
    this.users.addAccount({ idNum: '541', type: 'admin' } as User);
    this.users.addAccount({ idNum: '567', type: 'admin' } as User);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  /** Init notifications service / provider at the start of the application */
  private initNotifications() {
    this.authService.getAuthState$().subscribe((auth) => {
      if (auth) {
        this.saveNotificationToken(auth.uid);
      } else {
        this.saveNotificationToken();
      }
    });
  }

  /** Generate a new token & save it to the database (update if exists) each time the app is lunched */
  private saveNotificationToken(customerId?: string) {
    // Generate a new FCM Token for this device & save it to the database
    this.fcm.generateToken(customerId);
    // Display a toast message if a notification is received & the app is opened
    this.fcm.listenToNotifications().pipe(
      tap(msg => {
        alert(msg.body);
      })
    );
  }

}
