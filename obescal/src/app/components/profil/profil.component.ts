import { User } from './../../interfaces/User';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profil-comp',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
})

export class ProfilComponent implements OnInit {

  @Input() user: User;

  constructor() { }

  ngOnInit() { }

}
