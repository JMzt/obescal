import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/interfaces/User';
import { ToastController, ModalController } from '@ionic/angular';
import { NewUserPage } from 'src/app/modals/new-user/new-user.page';

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss'],
})

export class FormUserComponent implements OnInit {

  @Input() type: string;
  labelInput: string;
  newUser: User

  public users = [] as User[];

  constructor(private usersService: UsersService, private toastController: ToastController, private modalController: ModalController) {
    this.newUser = {} as User;
  }

  ngOnInit() {
    this.usersService.getUsersByType(this.type).then((users: User[]) =>{
      this.users = users;
    });/*
    switch (this.type) {
      case "student":
        this.labelInput = "Numéro étudiant";
        this.newUser.type = 'student';
        break;
      case "prof":
        this.labelInput = "Numéro professeur";
        this.newUser.type = 'professor';
        break;
      case "admin":
        this.labelInput = "Numéro admin";
        this.newUser.type = 'admin';
        break;
    }*/
    
  }

  public updateUserList(type: string) {
    this.usersService.getUsersByType(type).then((users: User[]) =>{
      this.users = users;
    });
  }

  public addUser() {
    // TODO : check if account already exists
    this.usersService.addAccount(this.newUser);
    this.presentToast();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Utilisateur crée',
      position: 'top',
      duration: 2000,
      animated: true
    });
    toast.present();
  }

  public deleteUser(user: User){
// TODO delete
  }

  public async addUserModal(type: string){
      const modal = await this.modalController.create({
        component: NewUserPage
      });
      modal.onDidDismiss().then(() => {
        this.updateUserList(this.type);
   });
      modal.present();
  }

}
