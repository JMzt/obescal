import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/interfaces/User';
import { Router } from '@angular/router';
import { PromotionsService } from 'src/app/services/promotions.service';
import { Promotion } from 'src/app/interfaces/Promotion';

@Component({
  selector: 'app-home-student-comp',
  templateUrl: './home-student.component.html',
  styleUrls: ['./home-student.component.scss'],
})

export class HomeStudentComponent implements OnInit {

  /** Contain the student data provided in entry  */
  @Input() student: User;

  constructor(private router: Router, private promotionService: PromotionsService) { }

  ngOnInit() {
    console.log(this.student)
  }

  public goToHistoric() {
    this.router.navigateByUrl('histo-calls');
  }

  public promoStudent(promo: string){
    return promo.split("_", 1);
  }
}
