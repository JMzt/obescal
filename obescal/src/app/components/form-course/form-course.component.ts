import { Component, OnInit } from '@angular/core';
import { Course } from 'src/app/interfaces/Course';
import { CoursesService } from 'src/app/services/courses.service';
import { ToastController, ModalController } from '@ionic/angular';
import { NewCoursePage } from 'src/app/modals/new-course/new-course.page';

@Component({
  selector: 'app-form-course',
  templateUrl: './form-course.component.html',
  styleUrls: ['./form-course.component.scss'],
})

export class FormCourseComponent implements OnInit {
  public courseName: string;
  public courses = [] as Course[];

  constructor(private courseService: CoursesService, private toastController: ToastController,
    private modalController: ModalController) { }

  ngOnInit() {
    this.updateListCourses();
  }

  public updateListCourses() {
    this.courseService.getCourses().then((courses: Course[]) => {
      this.courses = courses;
    });
  }
  public deleteCourse(c: Course) {
    this.courseService.removeCourse(c);
    this.updateListCourses();
  }

  public async addCourseModal() {
    let modal = await this.modalController.create({
      component: NewCoursePage
    });
    modal.onDidDismiss().then(() => {
        this.updateListCourses();
   });
    modal.present();
  }
}
