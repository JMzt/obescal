import { Promotion } from './../../interfaces/Promotion';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'firebase';

@Component({
  selector: 'app-register-comp',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})

export class RegisterComponent implements OnInit {

  /** Account data to fill */
  @Input() account: { user: User, pw: string };

  /** Promotion list to display */
  @Input() promotions: Promotion[];

  /** Selected promotion output */
  @Output() selectPromotion = new EventEmitter<Promotion>();

  constructor() { }

  ngOnInit() { }

}
