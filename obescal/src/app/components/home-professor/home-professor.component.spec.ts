import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeProfessorComponent } from './home-professor.component';

describe('HomeProfessorComponent', () => {
  let component: HomeProfessorComponent;
  let fixture: ComponentFixture<HomeProfessorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeProfessorComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeProfessorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
