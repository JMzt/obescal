import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/interfaces/User';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {AppelService} from '../../services/appel.service';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-home-professor-comp',
  templateUrl: './home-professor.component.html',
  styleUrls: ['./home-professor.component.scss'],
})

export class HomeProfessorComponent implements OnInit {

  @Input() professor: User;
  constructor(private router: Router, private geolocation: Geolocation, public alertCtrl: AlertController) { }

  ngOnInit() { }

  public goToNewAppel() {
    this.router.navigateByUrl('prof-appel');
  }

  public goToHistoCalls() {
    this.router.navigateByUrl('histo-calls-prof');
  }

  geolocate() {
    this.geolocation.getCurrentPosition({enableHighAccuracy: true}).then((resp) => {
      this.presentAlert(resp.coords.latitude, resp.coords.longitude);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  async presentAlert(lat: number, long: number) {
    const alert = await this.alertCtrl.create({
      header: 'Votre Position',
      subHeader: '',
      message: 'Longitude : ' + lat + '\nLatitude : ' + long,
      buttons : ['OK']
    });

    await alert.present();
  }
}
