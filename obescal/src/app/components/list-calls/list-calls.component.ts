import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Appel } from 'src/app/interfaces/Appel';
import { AppelService } from 'src/app/services/appel.service';
import { PromotionsService } from 'src/app/services/promotions.service';
import { Promotion } from 'src/app/interfaces/Promotion';
import { ModalController } from '@ionic/angular';
import { CallListAbsentsPage } from 'src/app/modals/call-list-absents/call-list-absents.page';

@Component({
  selector: 'app-list-calls',
  templateUrl: './list-calls.component.html',
  styleUrls: ['./list-calls.component.scss'],
})
export class ListCallsComponent implements OnInit {

  public calls = [] as Appel[];
  public callList;
  public promotions =[] as Promotion[];


  constructor(private callsService: AppelService, private promotionService: PromotionsService,
      private modalController: ModalController) { }

  ngOnInit() {
    /*this.callList = this.callsService.getAppelByPromo("L3MIAGE_1c6ca4fc-3af2-4af3-a9bc-0030f99b753d");
    this.callsService.getCalls().then((courses: Appel[]) => {
      this.calls = courses;
    });*/
    
    this.promotionService.getPromotions().then((promotions: Promotion[]) => {
      this.promotions = promotions;
    });
  }

  public selectPromotion(event){
      console.log(event)
      this.callsService.getCanceledCalls(event.detail.value).then((calls: any) => {
        console.log(calls);
        this.calls = calls;
      });
  }

  public openModalCall(call){
    console.log("click opene modal :: "+call.profName);
    this.presentFirstConnectModal(call);
  }

  /** present the student list modal */
  public async presentFirstConnectModal(appel:Appel) {
    console.log("presnet modal ::: "+appel.profName);
    const modal = await this.modalController.create({
      component: CallListAbsentsPage,
      componentProps: { call: appel }
    });
    modal.present();
/*
    modal.onDidDismiss().then((result) => {
      // Update the completed user data
      this.updateUserData(result.data);
    });*/
  }

}
