import { Component, OnInit } from '@angular/core';
import { Promotion } from 'src/app/interfaces/Promotion';
import { PromotionsService } from 'src/app/services/promotions.service';
import { ToastController, ModalController } from '@ionic/angular';
import { NewPromotionPage } from 'src/app/modals/new-promotion/new-promotion.page';

@Component({
  selector: 'app-form-promotion',
  templateUrl: './form-promotion.component.html',
  styleUrls: ['./form-promotion.component.scss'],
})

export class FormPromotionComponent implements OnInit {

  public promotionName: string;
  public promotions = [] as Promotion[];

  constructor(private promotionService: PromotionsService, private toastController: ToastController,
    private modalController: ModalController) { }

  ngOnInit() {
    this.updatePromotionList();
  }

  public updatePromotionList() {
    this.promotionService.getPromotions().then((promotions: Promotion[]) => {
      this.promotions = promotions;
    });
  }

  public deletePromo(promo: Promotion){
    this.promotionService.removePromotion(promo);
    this.updatePromotionList();
  }
  public async addPromotionModal(promo: Promotion) {
    const modal = await this.modalController.create({
      component: NewPromotionPage
    });
    modal.onDidDismiss().then(() => {
      this.updatePromotionList();
 });
    modal.present();
  }

}
