import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallStudentComponent } from './call-student.component';

describe('CallStudentComponent', () => {
  let component: CallStudentComponent;
  let fixture: ComponentFixture<CallStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CallStudentComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
