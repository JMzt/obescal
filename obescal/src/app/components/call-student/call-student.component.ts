import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Appel } from 'src/app/interfaces/Appel';
import { AppelService } from 'src/app/services/appel.service';
import { User } from 'src/app/interfaces/User';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ToolsService } from 'src/app/services/tools.service';
import { ToastController, ModalController } from '@ionic/angular';
import { ResponseCallStudentPage } from 'src/app/modals/response-call-student/response-call-student.page';

@Component({
  selector: 'app-call-student-comp',
  templateUrl: './call-student.component.html',
  styleUrls: ['./call-student.component.scss'],
})

export class CallStudentComponent implements OnInit {

  @Input() calls: Appel[];
  @Input() userId: string;
  @Output() answerToCall = new EventEmitter<{ call: Appel, present: boolean }>();

  constructor(private callsService: AppelService, private geolocation: Geolocation, private tools: ToolsService, private toastCtrl: ToastController, public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async imPresent(call: Appel) {
    this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(async (resp) => {
      const distance = this.tools.distanceInKmBetweenEarthCoordinates(call.latProf, call.longProf, resp.coords.latitude, resp.coords.longitude) * 1000;
      const modal = await this.modalCtrl.create({
        component: ResponseCallStudentPage,
        componentProps: { distance: distance, user: this.userId, call: call }
      });
      return await modal.present();
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  public isPresent(call: Appel): string {
    let res = 'false';
    call.studentsList.forEach((student: { student: User, present: boolean }) => {
      if (student.student && student.student.id === this.userId && student.present) {
        return res = 'Vous êtes marqué comme présent à ce cours';
      }
    });
    return res;
  }

  /** Display a toast message */
  private async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

}
