import { Component, OnInit, Input } from '@angular/core';
import { User } from 'firebase';

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.scss'],
})

export class HomeAdminComponent implements OnInit {

  @Input() admin: User;
  showAddCourse: boolean;
  showAddPromotion: boolean;
  showAddProf: boolean;
  showAddStudent: boolean;
  showAddAdmin: boolean;
  showCalls: boolean;
  formOn: boolean;

  constructor() { }

  ngOnInit() {
    this.formOn = false;
  }

  public clearForms() {
    this.showAddCourse = false;
    this.showAddPromotion = false;
    this.showAddStudent = false;
    this.showAddProf = false;
    this.showAddAdmin = false;
    this.showCalls = false;
    this.formOn = false;
  }

}
