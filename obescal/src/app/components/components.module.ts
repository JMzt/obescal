import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

// Obescal components
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfilComponent } from './profil/profil.component';
import { HomeStudentComponent } from './home-student/home-student.component';
import { HomeProfessorComponent } from './home-professor/home-professor.component';
import { HomeAdminComponent } from './home-admin/home-admin.component';
import { FormCourseComponent } from './form-course/form-course.component';
import { FormUserComponent } from './form-user/form-user.component';
import { FormPromotionComponent } from './form-promotion/form-promotion.component';
import { CallStudentComponent } from './call-student/call-student.component';
import { ListCallsComponent } from './list-calls/list-calls.component';

@NgModule({
    declarations: [LoginComponent, CallStudentComponent, HomeProfessorComponent, RegisterComponent, ProfilComponent, HomeStudentComponent, HomeAdminComponent, FormCourseComponent, FormUserComponent, FormPromotionComponent, ListCallsComponent],
    entryComponents: [],
    imports: [IonicModule, FormsModule, CommonModule],
    exports: [LoginComponent, CallStudentComponent, HomeProfessorComponent, RegisterComponent, ProfilComponent, HomeStudentComponent, HomeAdminComponent, FormCourseComponent, FormPromotionComponent, FormUserComponent, ListCallsComponent]
})
export class ComponentsModule { }
