import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
// Obescal
import { ModalConnectedStudentsPage } from './modal-connected-students.page';

const routes: Routes = [
  {
    path: '',
    component: ModalConnectedStudentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ModalConnectedStudentsPage]
})
export class ModalConnectedStudentsPageModule {}
