import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConnectedStudentsPage } from './modal-connected-students.page';

describe('ModalConnectedStudentsPage', () => {
  let component: ModalConnectedStudentsPage;
  let fixture: ComponentFixture<ModalConnectedStudentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalConnectedStudentsPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConnectedStudentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
