import { Component, Input, OnInit, ViewChild, OnChanges } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
// Obescal
import { AppelService } from '../../services/appel.service';
import { Appel } from '../../interfaces/Appel';

@Component({
  selector: 'app-modal-connected-students',
  templateUrl: './modal-connected-students.page.html',
  styleUrls: ['./modal-connected-students.page.scss'],
})

export class ModalConnectedStudentsPage implements OnInit, OnChanges {

  // "value" passed in componentProps
  @Input() generatedkey: string;
  @Input() appel: Appel;
  public student_list: any;

  constructor(navParams: NavParams, private modalController: ModalController, private appelService: AppelService) {
    // componentProps can also be accessed at construction time using NavParams
  }

  ngOnInit() {
    this.appelService.getAppelById(this.appel.id, this.appel.promo).subscribe((appel: Appel) => {
      if (appel && appel.studentsList) {
        this.student_list = appel.studentsList;
      }
    });
  }

  ngOnChanges() {
    this.ngOnInit();
  }

  public stopCall() {
    this.appel.studentsList = this.student_list;
    this.appelService.stopCurrentCall(this.appel).then(() => {
      console.log('fait');
    }).catch((e) => console.error(e));
    this.modalController.dismiss();
  }

  public closeModal() {
    this.appelService.stopCurrentCall(this.appel).then(() => {
      console.log('fait');
    }).catch((e) => console.error(e));
    this.modalController.dismiss();
  }

  private changeStudentState(student, $event) {
    student.present = $event.detail.checked;
    this.appel.studentsList = this.student_list;
    this.appelService.updateCall(this.appel).then(() => {
    });
  }

}
