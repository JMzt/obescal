import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfAppelPage } from './prof-appel.page';

describe('ProfAppelPage', () => {
  let component: ProfAppelPage;
  let fixture: ComponentFixture<ProfAppelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfAppelPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfAppelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
