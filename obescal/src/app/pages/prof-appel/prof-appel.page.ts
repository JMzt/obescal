import { Component, OnInit, Input } from '@angular/core';
import { ToolsService } from '../../services/tools.service';
import { ModalController, ToastController } from '@ionic/angular';
import { ModalConnectedStudentsPage } from '../modal-connected-students/modal-connected-students.page';
import { PromotionsService } from './../../services/promotions.service';
import { Promotion } from './../../interfaces/Promotion';
import { AppelService } from '../../services/appel.service';
import { Appel } from '../../interfaces/Appel';
import { Course } from '../../interfaces/Course';
import { CoursesService } from '../../services/courses.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { User } from 'src/app/interfaces/User';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-prof-appel',
  templateUrl: './prof-appel.page.html',
  styleUrls: ['./prof-appel.page.scss'],
})

export class ProfAppelPage implements OnInit {

  appel = {} as Appel;
  courses = [] as Course[];
  promotions = [] as Promotion[];
  students = [] as User[];
  latitude: number;
  longitude: number;
  userLogged: User;
  idUserLogged: string;

  constructor(private toolsService: ToolsService, public toastController: ToastController,
    public modalController: ModalController,
    private promotionService: PromotionsService,
    private appelService: AppelService, private courseService: CoursesService,
    private geolocation: Geolocation,
    private authService: AuthService,
    private userService: UsersService) { }

  ngOnInit() {
    this.authService.getAuthState$().subscribe((auth) => {
      if (auth) {
        this.userService.getUserById(auth.uid).then((user: User) => {
          this.userLogged = user;
        });
        this.promotionService.getPromotions().then((promotions: Promotion[]) => {
          this.promotions = promotions;
        });
        this.courseService.getCourses().then((courses: Course[]) => {
          this.courses = courses;
        });
        this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((resp) => {
          this.longitude = resp.coords.longitude;
          this.latitude = resp.coords.latitude;
        }).catch((error) => {
          console.log('Error getting location', error);
        });
      }
    });
  }

  async startNewCall() {
    this.appel.codeUnique = this.generateCodeCall();
    this.appel.profName = this.userLogged.name;
    this.appel.latProf = this.latitude;
    this.appel.longProf = this.longitude;
    this.appelService.createCall(this.appel);
    const modal = await this.modalController.create({
      component: ModalConnectedStudentsPage,
      componentProps: { generatedkey: this.appel.codeUnique, appel: this.appel }
    });
    return await modal.present();
  }

  public endCurrentCall() {
    this.appelService.stopCurrentCall(this.appel);
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000,
    });
    toast.present();
  }

  public generateCodeCall(): string {
    let code = '';
    for (let _i = 0; _i < 6; _i++) {
      code += (Math.floor(Math.random() * 10) + 0).toString();
    }
    return code;
  }

}
