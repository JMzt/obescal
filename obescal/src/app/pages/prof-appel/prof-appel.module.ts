import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
// Obescal
import { ProfAppelPage } from './prof-appel.page';

const routes: Routes = [
  {
    path: '',
    component: ProfAppelPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfAppelPage]
})
export class ProfAppelPageModule { }
