import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistoCallsProfPage } from './histo-calls-prof.page';

const routes: Routes = [
  {
    path: '',
    component: HistoCallsProfPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistoCallsProfPage]
})
export class HistoCallsProfPageModule {}
