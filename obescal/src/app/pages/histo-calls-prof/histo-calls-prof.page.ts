import { Component, OnInit } from '@angular/core';
import { Appel } from 'src/app/interfaces/Appel';
import { AppelService } from 'src/app/services/appel.service';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/interfaces/User';
import { ToolsService } from 'src/app/services/tools.service';
import { PromotionsService } from 'src/app/services/promotions.service';
import { Promotion } from 'src/app/interfaces/Promotion';

@Component({
  selector: 'app-histo-calls-prof',
  templateUrl: './histo-calls-prof.page.html',
  styleUrls: ['./histo-calls-prof.page.scss'],
})
export class HistoCallsProfPage implements OnInit {

  public closed_calls: Appel[];
  public currentUser: User;
  public promo: Promotion;
  public promotions = [] as Promotion[];

  constructor(private callsService: AppelService, private authService: AuthService, private usersService: UsersService, public tools: ToolsService,
    private promotionService: PromotionsService) { }

  ngOnInit() {
    this.authService.getAuthState$().subscribe((auth) => {
      if (auth) {
        this.usersService.getUserById(auth.uid).then((currentUser) => {
          this.currentUser = currentUser;
          console.log(currentUser);

        })
      }
    });

    this.promotionService.getPromotions().then((promotions: Promotion[]) => {
      this.promotions = promotions;
    });
  }

  showCalls(promotionId: string) {
    this.callsService.getCanceledCalls(promotionId).then((calls: any) => {
      console.log(calls)
      this.closed_calls = calls;
    })
  }
}
