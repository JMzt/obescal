import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoCallsProfPage } from './histo-calls-prof.page';

describe('HistoCallsProfPage', () => {
  let component: HistoCallsProfPage;
  let fixture: ComponentFixture<HistoCallsProfPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoCallsProfPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoCallsProfPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
