import { Component, OnInit } from '@angular/core';
import { AppelService } from '../../services/appel.service';
import { Appel } from '../../interfaces/Appel';
import { ToolsService } from '../../services/tools.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-reponse-appel',
  templateUrl: './student-reponse-appel.page.html',
  styleUrls: ['./student-reponse-appel.page.scss'],
})
export class StudentReponseAppelPage implements OnInit {

  appel: Appel;
  date: string;
  isValid = false;
  inputCode: string;
  longStu: number;
  latStu: number;

  constructor(private appelService: AppelService,
    private toolService: ToolsService,
    private geolocation: Geolocation,
    private alertController: AlertController,
    private router: Router,
    private toastController: ToastController) { }

  ngOnInit() {
    this.appelService.getAppelById('', '').subscribe((response) => {
      this.appel = response as Appel;
      this.date = this.toolService.formatDate(this.appel.startHour);
      this.appel.startHour = this.toolService.formatTime(this.appel.startHour);
      this.appel.endHour = this.toolService.formatTime(this.appel.endHour);
      console.log(this.toolService.formatDate(this.appel.endHour));
      console.log(response);
    }
    );
    this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((resp) => {
      this.latStu = resp.coords.latitude;
      this.longStu = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  changeValidValue() {
    if (this.isValid === false) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
  }

  public checkCall() {
    if (this.appel.codeUnique !== this.inputCode) {
      this.presentToast('Code invalide');
    } else if (this.toolService.distanceInKmBetweenEarthCoordinates(this.appel.latProf, this.appel.longProf, this.latStu, this.longStu)) {
      this.presentToast('Votre position ne permet pas de valider votre présence');
      // TODO: étudiant absent DB
      this.router.navigateByUrl('/home');
    } else {
      this.presentAlert();
      this.router.navigateByUrl('/home');
      // TODO : étudiant présent DB
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Présence validée',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      showCloseButton: true,
      position: 'middle',
      closeButtonText: 'Ok',
      animated: true
    });
    toast.present();
  }

}
