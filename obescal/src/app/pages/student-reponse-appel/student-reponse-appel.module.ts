import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { StudentReponseAppelPage } from './student-reponse-appel.page';

const routes: Routes = [
  {
    path: '',
    component: StudentReponseAppelPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StudentReponseAppelPage]
})
export class StudentReponseAppelPageModule { }
