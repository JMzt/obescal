import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentReponseAppelPage } from './student-reponse-appel.page';

describe('StudentReponseAppelPage', () => {
  let component: StudentReponseAppelPage;
  let fixture: ComponentFixture<StudentReponseAppelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StudentReponseAppelPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentReponseAppelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
