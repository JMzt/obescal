import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfSuiviAppelPage } from './prof-suivi-appel.page';

describe('ProfSuiviAppelPage', () => {
  let component: ProfSuiviAppelPage;
  let fixture: ComponentFixture<ProfSuiviAppelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfSuiviAppelPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfSuiviAppelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
