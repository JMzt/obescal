import { Component, OnInit } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { User } from './../../interfaces/User';
import { UsersService } from './../../services/users.service';
import { AppelService } from './../../services/appel.service';
import { ToastController, LoadingController, ModalController } from '@ionic/angular';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { FirstConnectPage } from 'src/app/modals/first-connect/first-connect.page';

@Component({
  selector: 'app-prof-suivi-appel',
  templateUrl: './prof-suivi-appel.page.html',
  styleUrls: ['./prof-suivi-appel.page.scss'],
})
export class ProfSuiviAppelPage implements OnInit {

  public currentUser = {} as User;
  public currentUsers = [] as User[];
  public testUser = {} as User;
  public presence = "miaou" as string;

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    console.log(this.usersService.getUserById("gVXX4iXbMeWly2FQyXvPBTI3L5l1"));

    this.usersService.getStudentList().then((users) => {
      this.currentUsers = users;
      console.log(users)
    }).catch((e) => {
      console.error(e)
    })
  }

  /** Get the presence status (boolean) */
  getPresenceStatus(user: User) {
    //console.log(user)
    //return true;
    return false;
  }

}
