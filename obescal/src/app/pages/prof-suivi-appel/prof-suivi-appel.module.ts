import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
// Obescal
import { ProfSuiviAppelPage } from './prof-suivi-appel.page';

const routes: Routes = [
  {
    path: '',
    component: ProfSuiviAppelPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfSuiviAppelPage]
})
export class ProfSuiviAppelPageModule { }
