import { AngularFireAuth } from 'angularfire2/auth';
import { User } from './../../interfaces/User';
import { UsersService } from './../../services/users.service';
import { ToastController, LoadingController, ModalController } from '@ionic/angular';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirstConnectPage } from 'src/app/modals/first-connect/first-connect.page';
import { Appel } from 'src/app/interfaces/Appel';
import { AppelService } from 'src/app/services/appel.service';
import { PromotionsService } from 'src/app/services/promotions.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

  public isLoggedIn;
  public userId: string;
  public currentUser = {} as User;
  public calls = [] as Appel[];

  constructor(public router: Router, private authService: AuthService, private toastCtrl: ToastController,
    private usersService: UsersService, private loadingCtrl: LoadingController, private modalCtrl: ModalController,
    private afAuth: AngularFireAuth, private callsService: AppelService, private promoService: PromotionsService) { }

  async ngOnInit() {
    const loading = await this.loadingCtrl.create({
      message: 'Chargement...',
      spinner: 'crescent'
    });
    loading.present();

    this.afAuth.authState.subscribe((auth) => {
      if (auth) {
        this.isLoggedIn = true;
        this.usersService.getUserById(auth.uid).then((userData: User) => {
          this.currentUser = userData;
          if (userData.type === 'student') {
            this.callsService.getAppelByPromo(this.currentUser.promo).subscribe((calls) => {
              this.calls = calls as Appel[];
            });
            loading.dismiss();
          } else {
            loading.dismiss();
          }
          if (this.currentUser.first_connect === undefined || this.currentUser.first_connect) {
            this.presentFirstConnectModal(this.currentUser);
          }
        }).catch((error) => {
          loading.dismiss();
        });
      } else {
        loading.dismiss();
        this.isLoggedIn = false;
        this.currentUser = undefined;
      }
    });
  }

  /** present the first connection modal form */
  public async presentFirstConnectModal(user: User) {
    const modal = await this.modalCtrl.create({
      component: FirstConnectPage,
      componentProps: { user: this.currentUser }
    });
    modal.present();

    modal.onDidDismiss().then((result) => {
      // Update the completed user data
      this.updateUserData(result.data);
    });
  }

  /** Logout the user */
  public logout(): void {
    this.authService.logout().then(() => {
      this.presentToast('Vous vous êtes déconnecté.');
    });
  }

  /** Display a toast message */
  private async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  /** Update the user data */
  public async updateUserData(user: User) {
    const loading = await this.loadingCtrl.create({
      message: 'Chargement...',
      spinner: 'crescent'
    });
    loading.present();
    user.first_connect = false;
    this.usersService.updateUserData(user).then((response) => {
      loading.dismiss();
      this.presentToast('Vos données ont bien été mise à jour.');
    }).catch((error) => {
      loading.dismiss();
      console.error(error);
      this.presentToast('Une erreur s\'est produite : error.message');
    });
  }
}
