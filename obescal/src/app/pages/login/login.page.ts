import { PromotionsService } from './../../services/promotions.service';
import { Promotion } from './../../interfaces/Promotion';
import { UsersService } from './../../services/users.service';
import { AuthService } from '../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  /** Url to the brand logo */
  public brandUrl = 'https://dewey.tailorbrands.com/production/brand_version_mockup_image/472/1844461472_7fac37e9-bd03-44a0-87a2-49b0454320c7.png?cb=1554111953' as string;
  /** Show or hide the register form */
  public showRegisterForm = false as boolean;
  /** Login or register account data  */
  public account = {} as { user: User, pw: string };
  public promotions = [] as Promotion[];
  public selected_promotion: Promotion;

  constructor(private authService: AuthService, private toastCtrl: ToastController, private userService: UsersService,
    private promotionService: PromotionsService, private router: Router, private loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.account.user = {} as User;
    this.account.pw = '';
    this.promotionService.getPromotions().then((promotions: Promotion[]) => {
      this.promotions = promotions;
    });
  }

  /** Loggin the user with the given email & pw */
  public login(email: string, pw: string): void {
    this.authService.signInWithEmailAndPassword(email, pw).then(() => {
      this.presentToast('Vous êtes connecté.');
      this.router.navigateByUrl('/home');
    }).catch((error) => {
      this.presentToast(error.message);
      console.error(error);
    });
  }

  /** Register the user with the given email & pw */
  public async register(user: User, pw: string) {
    const loading = await this.loadingCtrl.create({
      message: 'Chargement...',
      spinner: 'crescent'
    });
    loading.present();
    this.userService.accountExists(user).then((exists: boolean) => {
      if (exists) {
        this.authService.createUserWithEmailAndPassword(user.email, pw).then((auth) => {
          user.id = auth.user.uid;
          this.userService.createAccount(user).then(() => {
            this.presentToast('Votre compte a été crée.');
            this.router.navigateByUrl('/home');
            loading.dismiss();
          });
        }).catch((error) => {
          loading.dismiss();
          this.presentToast(error.message);
        }).then(() => {
        });
      } else {
        loading.dismiss();
        this.presentToast('Identifants non reconnues. Veuillez contacter votre administration.');
      }
    }).catch((error) => {
      console.error(error);
      this.presentToast(error.message);
    });
  }

  /** Display a toast message */
  private async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

}
