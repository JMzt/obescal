import { Component, OnInit } from '@angular/core';
import { Appel } from 'src/app/interfaces/Appel';
import { AppelService } from 'src/app/services/appel.service';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/interfaces/User';
import { ToolsService } from 'src/app/services/tools.service';

@Component({
  selector: 'app-histo-calls',
  templateUrl: './histo-calls.page.html',
  styleUrls: ['./histo-calls.page.scss'],
})

export class HistoCallsPage implements OnInit {

  public closed_calls: Appel[];
  public currentUser: User;

  constructor(private callsService: AppelService, private authService: AuthService, private usersService: UsersService, public tools: ToolsService) { }

  ngOnInit() {
    this.authService.getAuthState$().subscribe((auth) => {
      if (auth) {
        this.usersService.getUserById(auth.uid).then((currentUser) => {
          this.currentUser = currentUser;
          this.callsService.getCanceledCalls(this.currentUser.promo).then((calls: any) => {
            this.closed_calls = calls;
          })
        })
      }
    })
  }

  public isPresent(call: Appel): boolean {
    let res = false;
    call.studentsList.forEach((student: { student: User, present: boolean }) => {
      if (student.student && student.student.id === this.currentUser.id && student.present) {
        return res = true;
      }
    });
    return res;
  }

}
