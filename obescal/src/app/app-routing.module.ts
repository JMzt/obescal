import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FormCourseComponent } from './components/form-course/form-course.component';
import { FormPromotionComponent } from './components/form-promotion/form-promotion.component';
import { FormUserComponent } from './components/form-user/form-user.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'prof-appel', loadChildren: './pages/prof-appel/prof-appel.module#ProfAppelPageModule' },
  { path: 'prof-suivi-appel', loadChildren: './pages/prof-suivi-appel/prof-suivi-appel.module#ProfSuiviAppelPageModule' },
  { path: 'first-connect', loadChildren: './modals/first-connect/first-connect.module#FirstConnectPageModule' },
  { path: 'student-reponse-appel', loadChildren: './pages/student-reponse-appel/student-reponse-appel.module#StudentReponseAppelPageModule' },

  { path: 'new-course', component: FormCourseComponent },
  { path: 'new-promo', component: FormPromotionComponent },
  { path: 'new-user', component: FormUserComponent },

  { path: 'histo-calls', loadChildren: './pages/histo-calls/histo-calls.module#HistoCallsPageModule' },
  { path: 'call-list-absents', loadChildren: './modals/call-list-absents/call-list-absents.module#CallListAbsentsPageModule'},
  { path: 'new-promotion', loadChildren: './modals/new-promotion/new-promotion.module#NewPromotionPageModule' },
  { path: 'new-course', loadChildren: './modals/new-course/new-course.module#NewCoursePageModule' },
  { path: 'response-call-student', loadChildren: './modals/response-call-student/response-call-student.module#ResponseCallStudentPageModule' },
  { path: 'histo-calls-prof', loadChildren: './pages/histo-calls-prof/histo-calls-prof.module#HistoCallsProfPageModule' },
  { path: 'new-user', loadChildren: './modals/new-user/new-user.module#NewUserPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
