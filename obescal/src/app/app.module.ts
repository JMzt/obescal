import { ModalsModule } from './modals/modals.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Firebase } from '@ionic-native/firebase/ngx';
import { FormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from 'src/environments/environment';
import { ModalConnectedStudentsPage } from './pages/modal-connected-students/modal-connected-students.page';
import { FcmProvider } from './services/providers/fcm';
import { ResponseCallStudentPage } from './modals/response-call-student/response-call-student.page';

@NgModule({
  declarations: [AppComponent, ModalConnectedStudentsPage, ResponseCallStudentPage],
  entryComponents: [ModalConnectedStudentsPage, ResponseCallStudentPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ModalsModule,
    CommonModule,
    FormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    FcmProvider,
    Firebase,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
