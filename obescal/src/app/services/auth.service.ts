import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subject } from 'rxjs';

/** Service that manage authentifications */
@Injectable({
    providedIn: 'root',
})
export class AuthService {

    /** Id of the current connected user (annonymous or regular) */
    private userId = new Subject<string>();
    private loggedIn = new Subject<boolean>();
    private isloggedIn: boolean;
    private user: any;

    constructor(private afAuth: AngularFireAuth) {
        this.afAuth.authState.subscribe((Auth) => {
            if (Auth) {
                this.userId.next(Auth.uid);
                this.loggedIn.next(true);
                this.isloggedIn = true;
                this.user = Auth;
            } else {
                this.userId.next(null);
                this.loggedIn.next(false);
                this.isloggedIn = false;
                this.user = null;
            }
        });
    }

    public getAuthState$() {
        return this.afAuth.authState;
    }

    /** Return the user firebase data */
    public getUser(): any {
        return this.user;
    }

    /** Return the user firebase id */
    public getUserId(): any {
        if (!this.user) return;
        return this.user.uid;
    }

    /** Return the user firebase id as Observable. */
    public getUserId$(): Observable<string> {
        return this.userId;
    }

    /** Return the user logged in state as Observable. */
    public isLoggedIn$(): Observable<boolean> {
        return this.loggedIn;
    }

    /** Define if a user is logged in or not. */
    public isUserLoggedIn(): boolean {
        return this.isloggedIn;
    }

    /** logout the user. Return a promise. */
    public logout(): Promise<any> {
        return this.afAuth.auth.signOut();
    }

    /** Sing in the user anonymously. Return a promise. */
    public signInAnonymously(): Promise<any> {
        return this.afAuth.auth.signInAnonymously();
    }

    /** Create a new user with email & password. Return a promise. */
    public createUserWithEmailAndPassword(email: string, pw: string): Promise<any> {
        return this.afAuth.auth.createUserWithEmailAndPassword(email, pw);
    }

    /** Login with email & password. Return a promise. */
    public signInWithEmailAndPassword(email: string, pw: string): Promise<any> {
        return this.afAuth.auth.signInWithEmailAndPassword(email, pw);
    }

    /** For dev purposes only */
    public updateUserIdSubcriber_dev() {
        this.userId.next(this.user.uid);
    }
    
}
