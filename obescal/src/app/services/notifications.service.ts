import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { ToolsService } from './tools.service';

declare interface Device {
    userId: string;
    token: string;
}

declare interface Notification {
    id: string;
    title: string;
    message: string;
    userId: string;
    token: string;
}

/** Service that manage notifications */
@Injectable({
    providedIn: 'root',
})

export class NotificationsService {

    private devices_collection: AngularFirestoreCollection;
    private push_notifications_collection: AngularFirestoreCollection;

    constructor(private database: AngularFirestore, private toolsService: ToolsService) {
        this.devices_collection = this.database.collection<Device>('DEVICES');
        this.push_notifications_collection = this.database.collection<Device>('PUSH_NOTIFICATIONS');
    }

    /** Send a notification message to the user id given in entry */
    public sendNotificationToUserById(userId: string, message: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.devices_collection.doc(userId).get().toPromise().then((data) => {
                if (data.exists) {
                    const target_device = data.data() as Device;
                    const notification = { id: this.toolsService.generateUUID(), message: message, userId: target_device.userId, token: target_device.token } as Notification;
                    this.push_notifications_collection.doc(notification.id).set(notification).then(() => resolve());
                } else {
                    reject();
                }
            });
            resolve();
        });
    }

}
