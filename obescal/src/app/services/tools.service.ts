import { Injectable } from '@angular/core';
import * as uuidv4 from 'uuid';

@Injectable({
    providedIn: 'root',
})

export class ToolsService {

    constructor() { }

    public generateUUID(): string {
        return uuidv4();
    }

    public formatDate(date: string) {
        const d = new Date(date);
        return d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
    }

    public formatTime(date: string) {
        const d = new Date(date);
        return d.getHours() + 'H' + d.getMinutes();
    }

    /** Calculate the distance between two lat, lng points */
    public distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2): number {
        const earthRadiusKm = 6371;
        const dLat = this.degreesToRadians(lat2 - lat1);
        const dLon = this.degreesToRadians(lon2 - lon1);
        lat1 = this.degreesToRadians(lat1);
        lat2 = this.degreesToRadians(lat2);
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return parseInt((earthRadiusKm * c).toFixed(3));
    }

    /** Convert degrees to radians */
    private degreesToRadians(degrees) {
        return degrees * Math.PI / 180;
    }

}
