import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Course } from '../interfaces/Course';
import { ToolsService } from './tools.service';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  private courses_collection: AngularFirestoreCollection<Course>;

  constructor(private database: AngularFirestore, private toolsService: ToolsService) {
    this.courses_collection = this.database.collection<Course>('Courses');
  }

  /** Get all existing courses from the database as a Promise of course[] */
  public getCourses(): Promise<Course[]> {
    return new Promise((resolve, reject) => {
      this.courses_collection.get().toPromise().then((docs) => {
        const courses = [] as Course[];
        docs.forEach(element => {
          courses.push(element.data() as Course);
        });
        resolve(courses);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  /** Add a new course into the database */
  public createCourse(course: Course): Promise<any> {
    if (!course.id) course.id = course.name.replace(/\s/g, '') + '_' + this.toolsService.generateUUID();
    return this.courses_collection.doc(course.id).set(course);
  }

  /** Remove a course from the database */
  public removeCourse(course: Course): void {
    this.database.collection('Courses').doc(course.id).delete();
  }
  
}
