import { User } from './../interfaces/User';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { ToolsService } from './tools.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})

export class UsersService {

    private users_collection: AngularFirestoreCollection;

    constructor(private database: AngularFirestore, private toolsService: ToolsService) {
        this.users_collection = this.database.collection('Users');
    }

    /** Add a new initial account to the database */
    public addAccount(user: User): Promise<any> {
        if (!user.idNum) user.idNum = this.toolsService.generateUUID();
        if (!user.type) {
            console.log('ERROR : NO TYPE SPECIFIED FOR THE NEW USER');
            return;
        }
        user.first_connect = true;
        switch (user.type) {
            case 'student':
                return this.users_collection.doc('/STUDENTS').collection('/NEW_STUDENTS').doc(user.idNum).set(user);
            case 'professor':
                return this.users_collection.doc('/PROFESSORS').collection('/NEW_PROFESSORS').doc(user.idNum).set(user);
            case 'admin':
                return this.users_collection.doc('/ADMINS').collection('/NEW_ADMIN').doc(user.idNum).set(user);
        }
    }

    /** Check if an account already exist in the database */
    public accountExists(user: User): Promise<boolean> {
        let root;
        let where;
        switch (user.type) {
            case 'student':
                root = 'STUDENTS';
                where = 'NEW_STUDENTS';
                break;
            case 'professor':
                root = 'PROFESSORS';
                where = 'NEW_PROFESSORS';
                break;
            case 'admin':
                root = 'ADMINS';
                where = 'NEW_ADMIN';
                break;
        }
        return new Promise((resolve, reject) => {
            this.users_collection.doc('/' + root).collection('/' + where).doc(user.idNum).get().toPromise().then((doc) => {
                resolve(doc.exists);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /** Create a new accocunt depending of user type */
    public createAccount(user: User): Promise<any> {
        let from;
        let where;
        let root;
        switch (user.type) {
            case 'student':
                root = 'STUDENTS';
                from = 'NEW_STUDENTS';
                where = 'CREATED_STUDENTS';
                break;
            case 'professor':
                root = 'PROFESSORS';
                from = 'NEW_PROFESSORS';
                where = 'CREATED_PROFESSORS';
                break;
            case 'admin':
                root = 'ADMINS';
                from = 'NEW_ADMINS';
                where = 'CREATED_ADMINS';
                break;
        }
        return new Promise((resolve, reject) => {
            this.users_collection.doc('/' + root).collection('/' + from).doc(user.idNum).delete().then(() => {
                this.users_collection.doc('/' + root).collection('/' + where).doc(user.id).set(user).then(() => {
                    resolve();
                }).catch((error) => {
                    reject(error);
                });
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /** Get list of students as a promise */
    public getStudentList(): Promise<User[]> {
        return new Promise((resolve, reject) => {
            this.users_collection.doc('/STUDENTS').collection('/CREATED_STUDENTS').get().toPromise().then((doc) => {
                const tab = [];
                doc.docs.forEach((doc) => {
                    tab.push(doc.data());
                });
                resolve(tab);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /** Get a user data by id as a promise */
    public getUserById(userId: string): Promise<User> {
        let root;
        let from;
        return new Promise((resolve, reject) => {
            root = 'STUDENTS';
            from = 'CREATED_STUDENTS';
            this.users_collection.doc('/' + root).collection('/' + from).doc(userId).get().toPromise().then((doc) => {
                if (doc.exists) {
                    resolve(doc.data() as User);
                } else {
                    root = 'PROFESSORS';
                    from = 'CREATED_PROFESSORS';
                    this.users_collection.doc('/' + root).collection('/' + from).doc(userId).get().toPromise().then((doc) => {
                        if (doc.exists) {
                            resolve(doc.data() as User);
                        } else {
                            root = 'ADMINS';
                            from = 'CREATED_ADMINS';
                            this.users_collection.doc('/' + root).collection('/' + from).doc(userId).get().toPromise().then((doc) => {
                                if (doc.exists) {
                                    resolve(doc.data() as User);
                                } else {
                                    resolve();
                                }
                            });
                        }
                    });
                }
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /* Get all students from a promo*/
    public getStudentsByPromotion(promo: string): Promise<User[]> {
        return new Promise((resolve, reject) => {
            this.users_collection.doc('/STUDENTS').collection('/CREATED_STUDENTS').get().toPromise().then((docs) => {
                const users = [] as User[];
                docs.forEach(element => {
                    if (element.data().promo === promo) {
                        users.push(element.data() as User);
                    }
                });
                resolve(users);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /* Get all students from DB with promo = pomoId */
    public getStudentsByPromotionId(promoId: string): Promise<User[]> {
        return new Promise((resolve, reject) => {
            this.users_collection.doc('/STUDENTS').collection('/CREATED_STUDENTS').get().toPromise().then((docs) => {
                const users = [] as User[];
                docs.forEach(element => {
                    const curentUser = element.data() as User;
                    if (curentUser.promo === promoId) {
                        users.push(curentUser);
                    }
                });
            });
        });
    }

    /** Get a user data by id as an observable */
    public getUserById$(userId: string): Observable<User> {
        return new Observable(observer => {
            this.users_collection.doc('/STUDENTS').collection('/CREATED_STUDENT').doc(userId).get().subscribe((doc) => {
                observer.next(doc.data() as User);
            });
        });
    }

    /** Update the given user data in the dabase if exist */
    public updateUserData(user: User): Promise<any> {
        let from;
        let root;
        switch (user.type) {
            case 'student':
                root = 'STUDENTS';
                from = 'CREATED_STUDENTS';
                break;
            case 'professor':
                root = 'PROFESSORS';
                from = 'CREATED_PROFESSORS';
                break;
            case 'admin':
                root = 'ADMINS';
                from = 'CREATED_ADMINS';
                break;
        }
        return this.users_collection.doc('/' + root).collection('/' + from).doc(user.id).set(user);
    }

    public getUsersByType(type: string){
        let root;
        let from;
        switch (type){
            case 'student':
                root = 'STUDENTS';
                from = 'CREATED_STUDENTS';
                break;
            case 'professor':
                root = 'PROFESSORS';
                from = 'CREATED_PROFESSORS';
                break;
            case 'admin':
                root = 'ADMINS';
                from = 'CREATED_ADMINS';
                break;
        }
        return new Promise((resolve, reject) => {
            this.users_collection.doc('/'+root).collection('/'+from).get().toPromise().then((doc) => {
                const tab = [];
                doc.docs.forEach((doc) => {
                    tab.push(doc.data());
                });
                resolve(tab);
            }).catch((error) => {
                reject(error);
        });
    });
    
}}
