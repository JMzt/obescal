import { Injectable } from '@angular/core';
import { Appel } from '../interfaces/Appel';
import { ToolsService } from './tools.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { User } from '../interfaces/User';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})

export class AppelService {

  public checkedStudentList: Appel[];
  private appels_collection: AngularFirestoreCollection;

  constructor(public database: AngularFirestore, private toolsService: ToolsService, private usersService: UsersService) {
    this.appels_collection = this.database.collection('Calls');
  }

  /** Add a new initial professor call to the database */
  public createCall(call: Appel) {
    if (!call.id) call.id = this.toolsService.generateUUID();
    this.usersService.getStudentsByPromotion(call.promo).then((response) => {
      call.studentsList = [{}] as [{ student: User, present: boolean }];
      response.forEach((student) => {
        call.studentsList.push({ student: student, present: false });
      });
      return this.appels_collection.doc(call.promo).collection('opened_calls').doc(call.id).set(call);
    });
  }

  public getAppelByPromo(promoId: string) {
    return this.appels_collection.doc(promoId).collection('opened_calls').valueChanges();
  }

  public getAppelById(appelId: string, promoId: string) {
    return this.appels_collection.doc(promoId).collection('opened_calls').doc(appelId).valueChanges();
  }

  public getCanceledCalls(promoId: string) {
    return new Promise((resolve, reject) => {
      this.appels_collection.doc(promoId).collection('closed_calls').get().toPromise().then((docs) => {
        let tab = []
        docs.forEach((doc) => {
          tab.push(doc.data())
        });
        resolve(tab);
      });
    });
  }

  public stopCurrentCall(appel: Appel) {
    return this.appels_collection.doc(appel.promo).collection('opened_calls').doc(appel.id).delete().then(() => {
      this.appels_collection.doc(appel.promo).collection('closed_calls').doc(appel.id).set(appel);
    });
  }

  public getListOfPresentStudents(appel: Appel) {

  }

  answerToCall(call: Appel, present: boolean, userId: string) {
    call.studentsList.forEach((student) => {
      if (student.student && student.student.id === userId) {
        student.present = present;
      }
    });
    return this.appels_collection.doc(call.promo).collection('opened_calls').doc(call.id).set({ studentsList: call.studentsList }, { merge: true });
  }

  public updateCall(call: Appel) {
    return this.appels_collection.doc(call.promo).collection('opened_calls').doc(call.id).set(call);
  }
  
}
