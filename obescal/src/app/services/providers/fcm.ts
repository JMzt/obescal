import { Firebase } from '@ionic-native/firebase/ngx';
import { Platform } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';


@Injectable()
export class FcmProvider {

    private database_collection: AngularFirestoreCollection;

    constructor(public firebaseNative: Firebase, public database: AngularFirestore, private platform: Platform) {
        this.database_collection = this.database.collection<any>('DEVICES');
    }

    async generateToken(customerId?: string) {
        if (!this.platform.is('cordova')) return;

        let token;
        if (this.platform.is('android')) {
            token = await this.firebaseNative.getToken();
        }

        if (this.platform.is('ios')) {
            token = await this.firebaseNative.getToken();
            await this.firebaseNative.grantPermission();
        }
        return this.saveTokenToFirestore(token, customerId);
    }

    private saveTokenToFirestore(token, customerId?: string) {
        if (!token) return;

        if (customerId) {
            const registred_devices_ref = this.database_collection.doc('SAVED_DEVICES').collection('REGISTRED_DEVICES');
            registred_devices_ref.doc('/' + customerId).set({ token, customerId });
        }

        const all_devices_ref = this.database_collection.doc('SAVED_DEVICES').collection('ALL_DEVICES');
        return all_devices_ref.doc(token).set({ token });
    }

    public listenToNotifications() {
        return this.firebaseNative.onNotificationOpen();
    }
}
