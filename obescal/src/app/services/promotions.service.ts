import { ToolsService } from './tools.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { Promotion } from '../interfaces/Promotion';

@Injectable({
    providedIn: 'root',
})

export class PromotionsService {

    private promotions_collection: AngularFirestoreCollection<Promotion>;

    constructor(private database: AngularFirestore, private toolsService: ToolsService) {
        this.promotions_collection = this.database.collection<Promotion>('Promotions');
    }

    /** Get all existing promotions from the database as a Promise of promotion[] */
    public getPromotions(): Promise<Promotion[]> {
        return new Promise((resolve, reject) => {
            this.promotions_collection.get().toPromise().then((docs) => {
                const promotions = [] as Promotion[];
                docs.forEach(element => {
                    promotions.push(element.data() as Promotion);
                });
                resolve(promotions);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    public getPromotionById(id: string): Promise<Promotion> {
        return new Promise((resolve, reject) => {
            this.promotions_collection.doc(id).get().toPromise().then((data) => {
                if (data.exists) {
                    resolve(data.data() as Promotion);
                } else {
                    reject('Promotion inexistante.')
                }
            })
        });
    }

    /** Add a new promotion to the database */
    public createPromotion(promotion: Promotion): Promise<any> {
        if (!promotion.id) promotion.id = promotion.name.replace(/\s/g, '') + '_' + this.toolsService.generateUUID();
        return this.promotions_collection.doc(promotion.id).set(promotion);
    }

    /** Remove a promotion from the database */
    public removePromotion(promotion: Promotion) {
        this.promotions_collection.doc(promotion.id).delete();
    }

}
