export interface User {

    /** Firebase user id reference */
    id: string;
    name: string;
    first_name: string;
    email: string;
    phone: string;
    idNum?: string;

    /** Promotion if student */
    promo?: string;

    /** First connection or not of the account */
    first_connect: boolean;
    type: 'professor' | 'student' | 'admin';
}
