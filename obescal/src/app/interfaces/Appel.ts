import { User } from './User';

export interface Appel {
    id: string;
    codeUnique: string;
    profName: string;
    courseName: string;
    promo: string;
    startHour: string;
    endHour: string;
    studentsList: [{ student: User, present: boolean }];
    longProf: number;
    latProf: number;
}
