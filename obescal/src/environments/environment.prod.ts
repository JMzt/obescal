import { firebase_config } from './firebase.config';

export const environment = {
  production: true,
  firebase: firebase_config
};
